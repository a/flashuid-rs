# FlashUID-rs

Tool to flash UID section on UID modifiable Mifare Classic fobs with an ACR122U.

This is a Rust port of https://github.com/general-programming/flashuid, made to be slightly cleaner. Also I added significantly more verification to prevent flashing improper data (RIP [the fob I bricked before I wrote said verification features](https://elixi.re/i/027jx01z.png)).

Keep in mind that I'm not great with Rust, so expect some issues here and there with the code. Only tested on Linux.

## Usage

Plug in an ACR122U and run flashuid-rs:
- without any arguments to read the UID.
- with argument of uid or line 0 (1/4 of sector 0) to write the specified UID or line 0.
- with argument of "read" to read the whole fob and output contents to stdout.
- with argument of "clone" to read the line 0 (1/4 of sector 0) of the currently present fob, place a new fob and press return to clone a fob.

## Screenshot

![UID of an nfc fob being changed through the tool](https://elixi.re/i/2tuysepo.png)

## Shoutouts

Thanks a lot to linuxgemini for his continued help with my endless NFC hacking questions.

A bit of the code here is based on pcsc-rust's example, so huge shoutouts to the developer of that as well.

Also, thanks ACS for making amazing smartcard products that are actually affordable, and even more so for releasing SDKs and documentation of them for free.
