// FlashUID-RS: Tool written in Rust to flash UID section on UID modifiable Mifare Classic fobs with an ACR122U
// Copyright (C) 2019 Ave Ozkal

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

extern crate pcsc;
extern crate hex;

use std::env;
use pcsc::*;
use hex::FromHex;

// Source: Xpyder at https://users.rust-lang.org/t/how-to-get-user-input/5176/8
use simple_user_input::get_input;

mod simple_user_input {
    use std::io;
    pub fn get_input(prompt: &str) -> String{
        println!("{}",prompt);
        let mut input = String::new();
        match io::stdin().read_line(&mut input) {
            Ok(_goes_into_input_above) => {},
            Err(_no_updates_is_fine) => {},
        }
        input.trim().to_string()
    }
}

fn send_apdu(apdu: &[u8], card: &pcsc::Card) -> std::vec::Vec<u8> {
    let mut rapdu_buf = [0; MAX_BUFFER_SIZE];
    let rapdu = match card.transmit(apdu, &mut rapdu_buf) {
        Ok(rapdu) => rapdu,
        Err(err) => {
            eprintln!("Failed to transmit APDU command to card: {}", err);
            std::process::exit(1);
        }
    };
    // Hacky!
    rapdu.to_vec()
}

fn send_verify_apdu(apdu: &[u8], expected: &[u8], card: &pcsc::Card) -> std::vec::Vec<u8> {
    let mut rapdu_buf = [0; MAX_BUFFER_SIZE];
    let rapdu = match card.transmit(apdu, &mut rapdu_buf) {
        Ok(rapdu) => rapdu,
        Err(err) => {
            eprintln!("Failed to transmit APDU command to card: {}", err);
            std::process::exit(1);
        }
    };
    assert!(rapdu == expected, "rapdu ({:X?}) doesn't match expected value ({:X?})", rapdu, expected);
    // Hacky!
    rapdu.to_vec()
}

fn verify_uid(uid: &[u8]) {
    assert!(uid.len() >= 6, "UID must be at least 6 bytes long (currently {} bytes long).", uid.len());
    assert!(uid.len() <= 16, "UID must be at most 16 bytes long (currently {} bytes long).", uid.len());
    assert!(uid.len() < 6 || uid[5] == 0x08 || uid[5] == 0x88, "UID's 6th byte should be 0x08 or 0x88 (currently 0x{:02X?}).", uid[5]);
    assert!(uid.len() < 7 || uid[6] == 0x04, "UID's 7th byte should be 0x04 or must be omitted altogether (currently 0x{:02X?}).", uid[6]);
    assert!(uid.len() < 8 || uid[7] == 0x00, "UID's 8th byte should be 0x00 or must be omitted altogether (currently 0x{:02X?}).", uid[7]);
    let bcc = uid[0] ^ uid[1] ^ uid[2] ^ uid[3];
    assert!(bcc == uid[4], "Incorrect BCC (at byte 5, should be 0x{:02X?}, currently 0x{:02X?}).", bcc, uid[4]);
}

fn complete_uid(uid: &[u8], card: &pcsc::Card) -> std::vec::Vec<u8> {
    if uid.len() < 16 {
        let mut temp_uid = uid.to_vec();
        // Fetch block 0x00
        // On API Docs: "5.3. Read Binary Blocks"
        let mut first_block = send_apdu(b"\xFF\xB0\x00\x00\x10", &card);
        // Crop to 16 bytes (drop success bytes at end, a block is 16 bytes)
        first_block.truncate(16);
        // Delete UID bytes that are already provided by user
        first_block.drain(..temp_uid.len());
        // Form block with UID provided by user and data that's already in fob, return that
        temp_uid.extend(first_block);
        temp_uid
    }
    else {
        uid.to_vec()
    }
}

fn auth_witchcraft(card: &pcsc::Card) {
    // Commands taken from https://github.com/general-programming/flashuid/blob/master/index.js
    // Thanks linuxgemini!
    // I'll try to document these when I can
    send_verify_apdu(b"\xFF\x00\x00\x00\x08\xD4\x08\x63\x02\x00\x63\x03\x00", b"\xD5\x09\x90\x00", &card);
    send_verify_apdu(b"\xFF\x00\x00\x00\x06\xD4\x42\x50\x00\x57\xCD", b"\xD5\x43\x01\x90\x00", &card);
    send_verify_apdu(b"\xFF\x00\x00\x00\x05\xD4\x08\x63\x3D\x07", b"\xD5\x09\x90\x00", &card);
    send_verify_apdu(b"\xFF\x00\x00\x00\x03\xD4\x42\x40", b"\xD5\x43\x00\x0A\x90\x00", &card);
    send_verify_apdu(b"\xFF\x00\x00\x00\x05\xD4\x08\x63\x3D\x00", b"\xD5\x09\x90\x00", &card);
    send_verify_apdu(b"\xFF\x00\x00\x00\x03\xD4\x42\x43", b"\xD5\x43\x00\x0A\x90\x00", &card);
    send_verify_apdu(b"\xFF\x00\x00\x00\x08\xD4\x08\x63\x02\x80\x63\x03\x80", b"\xD5\x09\x90\x00", &card);
}

fn read_fob(card: &pcsc::Card, key: &[u8; 6]) {
    // TODO: Determine how many lines are available, 16 or 64.

    // Load the key into slot 0
    let mut apdu_command = b"\xFF\x82\x00\x00\x06".to_vec();
    apdu_command.extend(key.to_vec());
    send_verify_apdu(&apdu_command, b"\x90\x00", &card);

    // Read for 64 lines (16 sectors)
    for n in 0..64 {
        // Authenticate on the $n sector with key on slot 0
        apdu_command = b"\xFF\x86\x00\x00\x05\x01\x00".to_vec();
        apdu_command.extend([&n].to_vec());
        apdu_command.extend(b"\x60\x00".to_vec());

        send_verify_apdu(&apdu_command, b"\x90\x00", &card);

        // Read $n sector's data, for 0x10 (16) bytes
        apdu_command = b"\xFF\xB0\x00".to_vec();
        apdu_command.extend([&n].to_vec());
        apdu_command.extend(b"\x10".to_vec());

        let mut sectordata = send_apdu(&apdu_command, &card);
        sectordata.truncate(16);
        println!("Sector {} data: {}", n, hex::encode_upper(sectordata));
    }
}

fn read_sector(card: &pcsc::Card, key: &[u8; 6], sector: u8) -> std::vec::Vec<u8> {
    // Load the key into slot 0
    let mut apdu_command = b"\xFF\x82\x00\x00\x06".to_vec();
    apdu_command.extend(key.to_vec());
    send_verify_apdu(&apdu_command, b"\x90\x00", &card);

    // Authenticate on the $sector sector with key on slot 0
    apdu_command = b"\xFF\x86\x00\x00\x05\x01\x00".to_vec();
    apdu_command.extend([&sector].to_vec());
    apdu_command.extend(b"\x60\x00".to_vec());

    send_verify_apdu(&apdu_command, b"\x90\x00", &card);

    // Read $nsectsector sector's data, for 0x10 (16) bytes
    apdu_command = b"\xFF\xB0\x00".to_vec();
    apdu_command.extend([&sector].to_vec());
    apdu_command.extend(b"\x10".to_vec());

    let mut sectordata = send_apdu(&apdu_command, &card);
    sectordata.truncate(16);

    sectordata
}

fn disable_buzzer(card: &pcsc::Card) {
    // Disable buzzer
    // https://stackoverflow.com/a/41550221/3286892
    // On API Docs: "6.2. Bi-color LED and Buzzer Control"
    send_verify_apdu(b"\xFF\x00\x52\x00\x00", b"\x90\x00", &card);
}

fn write_uid(card: &pcsc::Card, mut uid: Vec<u8>) {
    verify_uid(&uid);

    // Run weird commands and bypass auth. Our best guess with linuxgemini is that this is a backdoor.
    auth_witchcraft(&card);

    // Complete UID by user to a whole block
    uid = complete_uid(&uid, &card).to_vec();

    // Form the command to replace block 0x00 (and therefore, UID)
    let uid_change_cmd = b"\xFF\x00\x00\x00\x15\xD4\x40\x01\xA0\x00";
    let mut full_cmd = uid_change_cmd.to_vec();
    full_cmd.extend(&uid);

    // Replace UID
    send_verify_apdu(&full_cmd, b"\xD5\x41\x00\x90\x00", &card);
    println!("Successfully replaced UID with {}. You may need to remove the fob from reader and place it again for it to be detected.", hex::encode_upper(&uid));
}

fn print_uid(card: &pcsc::Card) {
    // Get UID, print it for user
    // On API Docs: "4.1. Get Data"
    let mut out = send_apdu(b"\xFF\xCA\x00\x00\x00", &card);
    out.truncate(4);
    println!("Current UID: {}", hex::encode_upper(&out));
}

fn connect() -> pcsc::Card {
    // Establish a PC/SC context.
    let ctx = match Context::establish(Scope::User) {
        Ok(ctx) => ctx,
        Err(err) => {
            eprintln!("Failed to establish context: {}", err);
            std::process::exit(1);
        }
    };

    // List available readers.
    let mut readers_buf = [0; 2048];
    let mut readers = match ctx.list_readers(&mut readers_buf) {
        Ok(readers) => readers,
        Err(err) => {
            eprintln!("Failed to list readers: {}", err);
            std::process::exit(1);
        }
    };

    // Use the first reader.
    // TODO: Can we ensure that we pick an ACR122U, or at least not a Yubikey?
    let reader = match readers.next() {
        Some(reader) => reader,
        None => {
            println!("No readers are connected.");
            std::process::exit(1);
        }
    };
    println!("Using reader: {:?}", reader);

    // Connect to the card.
    let card = match ctx.connect(reader, ShareMode::Shared, Protocols::ANY) {
        Ok(card) => card,
        Err(Error::NoSmartcard) => {
            println!("A smartcard is not present in the reader.");
            std::process::exit(1);
        }
        Err(err) => {
            eprintln!("Failed to connect to card: {}", err);
            std::process::exit(1);
        }
    };

    disable_buzzer(&card);
    
    card
}

fn main() {
    println!("FlashUID-rs, developed by aveao, released under GPLv3.\nhttps://gitlab.com/ao/flashuid-rs\n");

    let card = connect();

    let args: Vec<String> = env::args().collect();

    match args.len() {
        2 => {
            // Get UID from user, verify it
            let arg: String = args[1].parse().expect("Invalid input (??? how)");

            if arg == "clone" {
                // TODO: add a way to detect key
                let sector0 = read_sector(&card, b"\xFF\xFF\xFF\xFF\xFF\xFF", 0x0);
                println!("Read sector 0: {}", hex::encode_upper(&sector0));

                get_input("Please place the second card and press return...");

                let card = connect();

                write_uid(&card, sector0);
            }
            else if arg == "read" {
                // TODO: add a way to detect key
                read_fob(&card, b"\xFF\xFF\xFF\xFF\xFF\xFF");
                // TODO: allow dumping to a file
            }
            else {
                print_uid(&card);

                let uid: Vec<u8> = Vec::from_hex(arg).expect("Input isn't a valid hex");

                write_uid(&card, uid);
            }
        },
        _ => {
            print_uid(&card);
            println!("No UID specified, therefore no UID was set.");
        }
    }
}
